FROM php:7.2-apache
COPY src/ /var/www/html/
COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer

RUN apt-get -y update
RUN apt-get -y install git zip

WORKDIR /var/www/html/
RUN composer install \
    --no-interaction \
    --no-plugins \
    --no-scripts \
    --no-dev \
    --prefer-dist

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
CMD ["apache2-foreground"]
