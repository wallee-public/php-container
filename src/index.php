<?php

require_once 'bootstrap.php';

?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8" />
		<title>${cdk.name}</title>
	</head>

	<body>
		<h1>${cdk.name}</h1>
		<p>Place your content here</p>
	</body>
</html>