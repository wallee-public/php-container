<?php

$spBaseUrl = 'https://'.getEnv('SERVICE_URL');
$serviceEntityId = getEnv('SP_ENTITY_ID');
$idpEntityId = getenv('IDP_ENTITY_ID');
$idpSsoUrl = getenv('IDP_SSO_URL');
$idpX509cert = getenv('IDP_X509_CERTIFICATE');

$settings = array (
	'debug' => true,
	'baseurl' => $spBaseUrl,
	'sp' => array (
		'entityId' => $spBaseUrl.'/metadata.php',
		'assertionConsumerService' => array (
			'url' => $spBaseUrl.'/index.php?acs',
		),
		'singleLogoutService' => array (
			'url' => $spBaseUrl.'/index.php?sls',
		),
		'NameIDFormat' => 'urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified',
	),
	'idp' => array (
		'entityId' => $idpEntityId,
		'singleSignOnService' => array (
			'url' => $idpSsoUrl,
		),
		// 'singleLogoutService' => array (
		// 	'url' => '',
		// ),
		'x509cert' => $idpX509cert,
	),
);
