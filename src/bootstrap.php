<?php 

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

function exception_handler($exception) {
	echo '<pre>';
	echo $exception->getMessage();
	echo "\n";
	echo $exception->getTraceAsString();
}
  
set_exception_handler('exception_handler');
  

require_once 'vendor/autoload.php';
require_once 'settings.php';

session_start();

use OneLogin\Saml2\Utils;
use OneLogin\Saml2\Auth;

if (isset($_GET['secret']) && $_GET['secret'] == STATIC_SECRET) {
	// When the static secret is set we directly login. We need that as the ScreenCloud can not 
	// authenticate with Okta / SAML.
	$_SESSION['samlNameId'] = "screen";
}
else {
	$auth = new Auth($settings);

	if (isset($_GET['logout'])) {
		unset($_SESSION['samlNameId']);
	}
	else if (isset($_GET['sso'])) {
		$auth->login();
	} else if (isset($_GET['acs'])) {
		if (isset($_SESSION) && isset($_SESSION['AuthNRequestID'])) {
			$requestID = $_SESSION['AuthNRequestID'];
		} else {
			$requestID = null;
		}

		$auth->processResponse($requestID);

		$errors = $auth->getErrors();

		if (!empty($errors)) {
			echo '<p>' . implode(', ', $errors) . '</p>';
			if ($auth->getSettings()->isDebugActive()) {
				echo '<p>'.$auth->getLastErrorReason().'</p>';
			}
		}

		if (!$auth->isAuthenticated()) {
			echo '<p>Not authenticated</p>';
			exit();
		}

		$_SESSION['samlUserdata'] = $auth->getAttributes();
		$_SESSION['samlNameId'] = $auth->getNameId();

		unset($_SESSION['AuthNRequestID']);
		header('Location: index.php');
	} 

	// Not logged in. So we need to redirect.
	if (!isset($_SESSION['samlNameId'])) {
		header('Location: ?sso');
		die();
	}
}
