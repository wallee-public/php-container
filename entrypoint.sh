#!/bin/bash

set -e

required_parameter_missing=false

if [ -z "$SERVICE_URL" ]; then
  echo "ERROR: Missing SERVICE_URL environment variable defining base URL of the application"
  required_parameter_missing=true;
fi
if [ -z "$SP_ENTITY_ID" ]; then
  echo "ERROR: Missing SP_ENTITY_ID environment variable (Entity ID of the service provider)"
  required_parameter_missing=true;
fi
if [ -z "$IDP_ENTITY_ID" ]; then
  echo "ERROR: Missing IDP_ENTITY_ID environment variable (Entity ID of the identity provider, e.g.: https://accounts.google.com/o/saml2?idpid=C034gd72h)"
  required_parameter_missing=true;
fi
if [ -z "$IDP_SSO_URL" ]; then
  echo "ERROR: Missing IDP_SSO_URL environment variable (SSO URL of the SAML identity provider, e.g.: https://accounts.google.com/o/saml2/idp?idpid=C034gd72h)"
  required_parameter_missing=true;
fi
if [ -z "$IDP_X509_CERTIFICATE" ]; then
  echo "ERROR: Missing IDP_X509_CERTIFICATE environment variable (Identity provider X509 Certificate, omit -----BEGIN CERTIFICATE----- and -----END CERTIFICATE----- boundaries and use only certificate data, e.g.: MIIDdDCCAlygAwIBAgIGAWtZsdgZMA0GCSqGSIb...)"
  required_parameter_missing=true;
fi
if [ "$required_parameter_missing" = true ] ; then 
  echo "ERROR: Container failed to start due to missing environment variables."
  exit 1
fi

printf '=%.0s' {1..80}
echo
echo "Service URL: $SERVICE_URL"
echo "Service entity ID: $SP_ENTITY_ID"
echo "Identity provider entity ID: $IDP_ENTITY_ID"
echo "SSO URL: $IDP_SSO_URL"
printf '=%.0s' {1..80} 
echo

# Start the regular process
docker-php-entrypoint "$@"